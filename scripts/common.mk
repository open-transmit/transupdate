PLATFORMS=linux/amd64,linux/arm64,linux/ppc64le,linux/386,linux/arm/v7,linux/arm/v6,linux/s390x 
# unsupported, alpine: linux/riscv64
DOCKER_NETWORK=sense8-net
DOCKER_REDIS_PORT=6379
ifndef TOPDIR 
	TOPDIR=.
endif

.PHONY: build
build: build-march

.PHONY: check-env
check-env:
ifndef DOCKERHUB_USER 
	$(error DOCKERHUB_USER is undefined)
endif

.PHONY: build-march
build-march: check-env setup-buildx
	$(info "BUILDX_OPTS: ${BUILDX_OPTS}")
	docker buildx build \
		$(BUILDX_OPTS) \
		--progress  plain \
		--compress \
		--push \
		--platform $(PLATFORMS) \
		--tag $(DOCKERHUB_USER)/$(TAG) .

.PHONY: shell
shell:
	docker run -it $(DOCKERHUB_USER)/$(TAG) /bin/sh

.PHONY: docker-create-network
docker-create-network:
	$(TOPDIR)/scripts/docker-network-create $(DOCKER_NETWORK)

.PHONY: docker-remove-network
docker-remove-network:
	$(TOPDIR)/scripts/docker-network-remove $(DOCKER_NETWORK)

.PHONY: docker-run docker-build
docker-run: docker-create-network
	docker run -it --network $(DOCKER_NETWORK) $(DOCKERHUB_USER)/$(TAG) $(CMD)

.PHONY: docker-build-local
docker-build-local: check-env
	docker build \
		--tag $(DOCKERHUB_USER)/$(TAG) \
		.

.PHONY: setup-buildx
setup-buildx:
# $(shell docker use sense8 && echo 1) && 
# $(shell docker buildx create --name sense8 --platform ${PLATFORMS} 2>/dev/null)
	docker buildx use sense8

.PHONY: clean
clean:
	rm -rf env/
	find . -name "*.log" -exec rm -f {} \;
	find . -name "*.pyc" -exec rm -f {} \;

env:
	cd $(TOPDIR)
	python3 -m venv env
	. env/bin/activate; \
		python -m pip install --upgrade pip; \
		pip install -r requirements.txt

.PHONY: activate
activate:
	@echo source env/bin/activate

.PHONY: deactivate
deactivate:
	@echo deactivate