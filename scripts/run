#!/usr/bin/env python3

import os
import sys

basepath = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
sys.path.insert(0, basepath)

if __name__ == "__main__":
    import asyncio
    import logging

    import requests
    from transupdate.options import Options
    from transupdate.run import run

    def configure_logging(options: Options):
        opts = {"level": logging.WARNING}
        hide_libraries_logging = lambda: [
            logging.getLogger(name).setLevel(logging.WARNING)
            for name in ["pyunpack", "urllib3.connectionpool", "asyncio"]
        ]
        if options.debug:
            opts["level"] = logging.DEBUG
        if options.logfile is not None:
            opts["filename"] = options.logfile
        logging.basicConfig(**opts)
        hide_libraries_logging()

    options = Options()
    configure_logging(options)
    from transupdate.logger import getLogger

    logger = getLogger(__file__)
    try:
        asyncio.run(run(options))
    except asyncio.exceptions.CancelledError:
        logger.debug("terminated")
    except KeyboardInterrupt:
        logger.debug("terminated")
    except Exception as e:
        # if options.debug:
        #     raise e
        logger.error(e)
