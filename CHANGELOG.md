# Changelog

## [Unreleased]

### Added

- JSON schema for config and validation `transupdate/schemas`
- Dockerfile: `Transmission` / `Transupdate`
- Options object from env/arguments/config
- Command line arguments parsing
- Daemon mode that execute task at given frequency
- Parsers loading at runtime
- Feed filters setting split by accept/rejects
