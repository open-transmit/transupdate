TOPDIR=.
include $(TOPDIR)/scripts/common.mk

TAG=transupdate-torrent-daemon:0.0.1

.PHONY: docker-test-run
docker-test-run:
	mkdir -p $(TOPDIR)/env/transmission-etc
	cp -r $(TOPDIR)/etc/transmission $(TOPDIR)/env/transmission-etc/transmission
	cp -r $(TOPDIR)/etc/transupdate.json $(TOPDIR)/env/transmission-etc/transupdate.json
	TRANSUPDATE_CONF=etc/transupdate.json \
	TRANSUPDATE_DEBUG=1 \
	TRANSUPDATE_LOGFILE=transupdate.log \
	TRANSUPDATE_DRY=1 \
	DOCKER_RUN_OPTS="-v $(shell pwd)/transupdate/:/transupdate/transupdate -v $(shell pwd)/env/transmission-etc/transmission:/etc/transmission -v $(shell pwd)/env/transmission-etc/transupdate.json:/etc/transupdate.json" \
	$(MAKE) docker-run

env/data/all-torrents.zip: env
	mkdir -p env/data
	wget "https://dl.armbian.com/torrent/all-torrents.zip" -O $@
	echo "python3 -m http.server --directory transupdate/env/data/ 8000 &"

.PHONY: armbian
armbian: env/data/all-torrents.zip

.PHONY: vulture
vulture: env
	. env/bin/activate; \
	pip install vulture

.PHONY: deadcode
deadcode: vulture
	. env/bin/activate; \
	python -m vulture \
	--ignore-decorators "@decorator)" \
	scripts/run transupdate/

.PHONY: docker-run
docker-run: docker-create-network
ifndef TRANSUPDATE_DATA
	$(info ">>> You should specify host path for torrents download")
	$(info ">>> For example:")
	$(info ">>> TRANSUPDATE_DATA=<...> make run)
	$(error >>> Error: TRANSUPDATE_DATA is undefined)
endif
	mkdir -p $(TRANSUPDATE_DATA)
	docker run \
		$(DOCKER_RUN_OPTS) \
		-v ${TRANSUPDATE_DATA}:/transupdate/data \
		-p 9091:9091 \
		-p 51413:51413 \
		--network $(DOCKER_NETWORK) \
		-it $(DOCKERHUB_USER)/$(TAG) $(CMD)

.PHONY: docker-clean
docker-clean:
	docker image rm -f $(DOCKERHUB_USER)/$(TAG)

.PHONY: test-run
test-run: env
	scripts/run