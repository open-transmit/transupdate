import logging
import os

name_from_file = lambda file: os.path.splitext(os.path.basename(file))[0]


def getLogger(file, raw=False):
    name = file
    if not raw:
        name = name_from_file(file)
    return logging.getLogger("TRANSUPDATE {: <20}".format(name))
