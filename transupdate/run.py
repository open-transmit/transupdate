import asyncio
import time

from transupdate.context import Context
from transupdate.feeds.common import feed_need_update, feed_set_nexupdate
from transupdate.logger import getLogger, name_from_file
from transupdate.options import Options, get_feeds
from transupdate.updater import updater
from transupdate.utils import getdctval, getnow

logger = getLogger(__file__)


async def worker(name: str, ctx: Context, queue: asyncio.Queue):
    logger = getLogger(
        "{}/{} {}".format(name_from_file(__file__), "worker", name), raw=True
    )
    while True:
        feed, now = await queue.get()
        if not feed_need_update(feed, now):
            queue.task_done()
            continue
        try:
            await updater(ctx, feed, now)
            feed_set_nexupdate(feed, now)
        except Exception as e:
            logger.error("Error: {}".format(e))
        queue.task_done()


async def run_helper(ctx: Context, queue: asyncio.Queue, now: float):
    for feed in get_feeds(ctx.options):
        queue.put_nowait((feed, now))
    logger.debug("feed count: {}".format(queue.qsize()))


async def run(options: Options):
    ctx = Context(options)
    queue = asyncio.Queue()
    started_at = getnow()
    alive = True
    tasks = []
    for i in range(0, options.workers):
        task = asyncio.create_task(worker(f"wid-{i}", ctx, queue))
        tasks.append(task)
    gather = asyncio.gather(*tasks, return_exceptions=False if options.debug else True)
    while alive:
        now = getnow()
        await run_helper(ctx, queue, getnow())
        await queue.join()
        elapsed = now - getnow()
        if elapsed > 0:
            since = time.monotonic() - started_at
            logger.debug("[{:2f}] elapsed: {:10.2f} s".format(since, elapsed))
        alive = True if options.daemon else False
        if alive:
            time.sleep(options.sleep)
    for task in tasks:
        task.cancel()
    await gather
