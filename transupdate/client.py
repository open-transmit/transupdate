from clutch import Client

from transupdate.logger import getLogger
from transupdate.options import Options

logger = getLogger(__file__)

TORRENT_STATUS = ["STOPPED", "SEED", "CHECK", "CHECK_WAIT", "DOWNLOAD", "DOWNLOAD_WAIT"]
TORRENT_STATUS_ACTIVE = ["SEED", "CHECK", "CHECK_WAIT", "DOWNLOAD", "DOWNLOAD_WAIT"]


class FilterFieldError(Exception):
    def __init__(self, field, value):
        self.field = field
        self.value = value
        super(FilterFieldError, self).__init__("{}={}".format(field, value))


def validate_field(field, value):
    if field == "status" and value not in TORRENT_STATUS:
        raise FilterFieldError(field, value)


def is_filtered(torrent, filters=None):
    if filters is None:
        return False
    for field, values in filters.items():
        for value in values:
            validate_field(field, value)
            field_value = getattr(torrent, field).name
            if field_value == value:
                return False
    return True


def make_client(options: Options):
    return Client(address=options.transmission["address"])


def get(ctx, fields, filters=None, all_fields=False):
    response = ctx.client.torrent.accessor(fields, all_fields=all_fields)
    for torrent in response.arguments.torrents:
        if is_filtered(torrent, filters):
            continue
        yield response, torrent


def add(ctx, filename=None, paused=False, metainfo=None):
    params = {"paused": paused}
    if metainfo is not None:
        params.update({"metainfo": metainfo})
    else:
        params.update({"filename": filename})

    def _pretty():
        return (
            filename
            if filename is not None
            else "metainfo: {}".format("<base64 torrent>")
        )

    logger.debug("adding {} paused={}".format(_pretty(), paused))
    return ctx.client.torrent.add(params)


def remove(ctx, torrent_id, delete_local_data=False):
    logger.debug(
        "removing id={} delete_local_data={}".format(torrent_id, delete_local_data)
    )
    return ctx.client.torrent.remove(torrent_id, delete_local_data=delete_local_data)
