from typing import Generator

from transupdate.context import Context
from transupdate.feeds.decorator import parsers
from transupdate.feeds.plugins import load_plugin
from transupdate.logger import getLogger
from transupdate.symbols import KEY_NEXTUPDATE
from transupdate.utils import getdctval

logger = getLogger(__file__)


def feed_need_update(feed, now) -> bool:
    if KEY_NEXTUPDATE not in feed:
        return True
    diff = feed[KEY_NEXTUPDATE] - now
    if diff <= 0:
        return True
    return False


def feed_set_nexupdate(feed, now) -> None:
    if KEY_NEXTUPDATE not in feed:
        feed[KEY_NEXTUPDATE] = 0
    else:
        feed[KEY_NEXTUPDATE] = now + getdctval(feed, "frequency", 10)


class TorrentFeedItem:
    def __init__(self, name, filename=None, metainfo=None):
        self.name = name
        self.filename = filename
        self.metainfo = metainfo

    def __str__(self):
        return "{} name: {}, href: {}".format(
            self.__class__.__name__,
            self.name,
            self.filename,
            True if self.metainfo else False,
        )

    def serialize(self):
        return {"filename": self.filename, "metainfo": self.metainfo}



def get_feed(ctx: Context, feed: any) -> Generator[TorrentFeedItem, None, None]:
    load_plugin(feed["kind"])
    return parsers[feed["kind"]](ctx, feed)