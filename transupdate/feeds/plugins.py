import os

from transupdate.config import basedir
from transupdate.logger import getLogger

logger = getLogger(__file__)


import importlib.util
import sys

__LOADED__ = {}


def import_module(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module


def load_plugin(name):
    if name not in __LOADED__:
        module_name = "transupdate_plugin_{}".format(name)
        file_path = os.path.join(
            basedir, "transupdate", "feeds", "plugins", name, "main.py"
        )
        module = import_module(module_name, file_path)
        __LOADED__[name] = module
    return __LOADED__[name]
