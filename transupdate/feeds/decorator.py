from transupdate.logger import getLogger

logger = getLogger(__file__)

parsers = {}


def feed_parser(kind=None):
    """store feed parser in dict[parsers]"""

    def decorator(fn):
        logger.debug("registering {}".format(kind))
        parsers[kind] = fn

    return decorator
