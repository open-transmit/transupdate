import os
import re
from typing import Generator
from urllib.parse import urljoin, urlparse

from bs4 import BeautifulSoup
from transupdate.context import Context
from transupdate.feeds import decorator
from transupdate.feeds.common import TorrentFeedItem
from transupdate.filters import filter_by_name, prepare_filters
from transupdate.logger import getLogger
from transupdate.utils import http_get, slice_array

logger = getLogger(__file__)

remove_query_string = lambda url: urljoin(url, urlparse(url).path)
filter_by_name_helper = lambda item, filters: filter_by_name(
    os.path.basename(item["href"]).strip(), filters
)


@decorator.feed_parser(kind="html")
def get_feed(ctx: Context, feed: any) -> Generator[TorrentFeedItem, None, None]:
    soup = BeautifulSoup(http_get(feed["url"]), "html.parser")
    filters = prepare_filters(feed)
    lambda_filter = lambda item: filter_by_name_helper(item, filters)
    links = filter(lambda_filter, soup.find_all("a", href=True))
    baseurl = remove_query_string(feed["url"])
    for idx, link in slice_array(links, feed["slice"]):
        href = link["href"].strip()
        name = os.path.basename(href)
        filename = href
        if not href.startswith("http"):
            filename = os.path.join(baseurl, href)
        yield TorrentFeedItem(name=name, filename=filename)
