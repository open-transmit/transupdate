import asyncio
import os
import tempfile
import time

import feedparser
import requests
from pyunpack import Archive
from transupdate.feeds import decorator
from transupdate.feeds.common import TorrentFeedItem
from transupdate.logger import getLogger
from transupdate.utils import http_get, readfile_tob64, slice_array

logger = getLogger(__file__)


@decorator.feed_parser(kind="archive")
def get_feed(ctx, feed):
    url = feed["url"]
    if "debug_url" in feed:
        url = feed["debug_url"]
    basename = os.path.basename(url)
    with tempfile.TemporaryDirectory() as tmpdirname:
        fn = os.path.join(tmpdirname, basename)
        with requests.get(url, stream=True) as response:
            with open(fn, "wb") as wh:
                for byte in response:
                    wh.write(byte)
        Archive(fn).extractall(tmpdirname)
        for fn in os.listdir(tmpdirname):
            if not fn.endswith(".torrent"):
                continue
            filename = os.path.join(tmpdirname, fn)
            if not os.path.exists(filename):
                logger.error("file not found {}".format(filename))
                continue
            name = os.path.splitext(os.path.basename(fn))[0]
            metainfo = readfile_tob64(filename)
            yield TorrentFeedItem(name=name, metainfo=metainfo)
