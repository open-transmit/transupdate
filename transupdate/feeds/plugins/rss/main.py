import os

import feedparser
from transupdate.feeds import decorator
from transupdate.feeds.common import TorrentFeedItem
from transupdate.logger import getLogger
from transupdate.utils import slice_array

logger = getLogger(__file__)


def filter_torrent_feed(ctx, feed, torrent) -> TorrentFeedItem:
    return torrent


@decorator.feed_parser(kind="rss")
def get_feed(ctx, feed):
    fp = feedparser.parse(feed["url"])
    for idx, entry in slice_array(fp.entries, feed["slice"]):
        filename = entry.links[0].href
        name = os.path.splitext(os.path.basename(filename))[0]
        torrent = filter_torrent_feed(
            ctx, feed, TorrentFeedItem(name=name, filename=filename)
        )
        if torrent is None:
            continue
        yield torrent
