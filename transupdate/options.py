import argparse
import json
import os
import sys
import time
import uuid

from transupdate.config import read_config
from transupdate.logger import getLogger
from transupdate.symbols import KEY_NEXTUPDATE
from transupdate.utils import getenv, listenv, resolve_path

logger = getLogger(__file__)

prettyopt = lambda key, value: "{}:{}".format(key, value)

known_ids = []
frequency_minute = lambda mn=10: int(mn * 60)
frequency_hour = lambda hour=1: frequency_minute(hour * 60)
frequency_day = lambda day=1: frequency_hour(day * 24)


def gen_id():
    make_id = lambda: uuid.uuid4().hex[:6]
    newid = make_id()
    while newid in known_ids:
        newid = make_id
    known_ids.append(newid)
    return newid


class OptionsError(Exception):
    def __init__(self, msg, option=None, value=None):
        super(OptionsError, self).__init__(
            "{}\n\n{}{}".format(
                msg, option, "={}".format(value) if value is not None else ""
            )
        )


def make_kind_from_key(key):
    if key in ["debug", "daemon"]:
        return bool
    elif key in ["workers"]:
        return int
    return str


class Options:
    env_prefix = "TRANSUPDATE"
    env_variables = [
        "confpath",
        "daemon",
        "data",
        "debug",
        "dry",
        "logfile",
        "root",
        "user",
    ]

    def __init__(self, confpath=None):
        self.config = self.parse(confpath)
        self.sanity()
        self.init_daemon()

    def init_daemon(self):
        if not self.daemon:
            return
        now = time.time()
        for feed in self.feeds:
            if "id" in feed:
                continue
            feed["_id"] = gen_id()
            feed["frequency"] = (
                feed["frequency"] if "frequency" in feed else frequency_day()
            )
            feed[KEY_NEXTUPDATE] = 0
        key = "_daemon"
        if key not in self.config:
            self.config[key] = {"ids": known_ids}

    def make_env(self, key):
        if key not in self.env_variables:
            raise OptionsError("UnknownEnv<{}>".format(key))
        return "{}_{}".format(self.env_prefix, key.upper())

    def parse(self, confpath):
        gen_config = {}
        parser = parse_arguments(self)
        for key in self.env_variables:
            value = getattr(parser, key) if hasattr(parser, key) else None
            if value is not None:
                gen_config[key] = value
        for key in self.env_variables:
            kind = make_kind_from_key(key)
            value = getenv(self.make_env(key), None, kind=kind)
            if value is not None:
                gen_config[key] = value
        if "confpath" in gen_config and "~" in gen_config["confpath"]:
            gen_config["confpath"] = resolve_path(gen_config["confpath"])
        config = read_config(
            gen_config["confpath"] if "confpath" in gen_config else None
        )
        config.update(gen_config)
        return config

    def sanity(self):
        for key, value in listenv():
            if not key.startswith(self.env_prefix):
                continue
            _, real_key = [p.strip().lower() for p in key.split("_")]
            if real_key not in self.env_variables:
                logger.debug("\n\n# unset {}\n".format(key))
                raise OptionsError("EnvironmentalPollution", key)

    def __getattr__(self, key):
        if key in self.config:
            return self.config.get(key)
        if key in self.env_variables:
            return None
        raise OptionsError("UnknownOptions", option=key)

    def __str__(self):
        opts = "\n".join([prettyopt(k, v) for k, v in self.__dict__.items()])
        return """[{}]
        {}
        """.format(
            self.__class__.__name__, opts
        )


def get_feeds(options: Options, enabled: bool = True):
    for feed in options.feeds:
        _e = feed["enabled"] if "enabled" in feed else True
        if _e != enabled and _e is not None:
            continue
        yield feed


def parse_arguments(options: Options):
    parser = argparse.ArgumentParser(description="Transupdate Arguments Parser")
    for name in options.env_variables:
        kind = make_kind_from_key(name)
        if kind == bool:
            parser.add_argument("--{}".format(name), default=False, action="store_true")
        else:
            parser.add_argument("--{}".format(name), default=None)
    return parser.parse_args()
