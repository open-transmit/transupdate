import time

from transupdate.client import TORRENT_STATUS_ACTIVE, add, get, make_client, remove
from transupdate.config import read_config
from transupdate.options import Options

default_fields = {"id", "status", "name"}


class Context:
    def __init__(self, options: Options):
        self.options = options
        self._client = None

    @property
    def client(self):
        if self._client is None:
            self._client = make_client(self.options)
        return self._client

    def ls(
        self,
        fields=default_fields,
        filters={"status": TORRENT_STATUS_ACTIVE},
        all_fields=False,
    ):
        return get(self, fields=fields, filters=filters, all_fields=all_fields)

    def add(self, filename=None, paused=False, metainfo=None):
        return add(self, filename=filename, paused=paused, metainfo=metainfo)

    def remove(self, torrent_id, delete_local_data=False):
        return remove(self, torrent_id, delete_local_data)

    def __str__(self):
        return """[{}]
        options:
        {}
        """.format(
            self.__class__.__name__, str(self.options)
        )
