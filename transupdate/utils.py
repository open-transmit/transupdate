import base64
import json
import os
import time

import requests

from transupdate.logger import getLogger

logger = getLogger(__file__)


class HttpGetError(Exception):
    pass


def read_json(json_path: str) -> any:
    with open(json_path, "r", encoding="utf8") as rh:
        return json.load(rh)


def slice_array(array: iter, slice_str: str = None):
    start, end = [int(value) if value else None for value in slice_str.split(":")]
    array = list(array) if type(array) != "list" else array
    idx = -1
    for item in array[start:end]:
        idx += 1
        yield idx, item


def http_get(url, kind="text"):
    response = requests.get(url)
    if response.status_code != 200:
        raise HttpGetError(response.status_code)
    return getattr(response, kind)


def readfile_tob64(filepath) -> bytes:
    with open(filepath, "rb") as rh:
        return base64.b64encode(rh.read())


def getenv(key, default=None, kind=str):
    value = os.getenv(key, default)
    value = None if value == "" else value
    if value is None:
        return default
    return kind(value)


def resolve_path(path):
    return os.path.abspath(os.path.expanduser(path))


listenv = lambda: os.environ.items()
getnow = lambda: time.monotonic()
getdctval = lambda dct, key, default=None: dct[key] if dct[key] is not None else default
