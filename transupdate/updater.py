import json
import os
import re

from transupdate.context import Context
from transupdate.feeds.common import TorrentFeedItem, get_feed
from transupdate.filters import (
    is_section_filtered,
    merge_filters_section,
    prepare_filters,
)
from transupdate.logger import getLogger
from transupdate.options import get_feeds

logger = getLogger(__file__)


def find_torrent(torrents: [TorrentFeedItem], filters={}):
    idx = -1
    for torrent in torrents:
        idx += 1
        value = getattr(torrent, "name")
        if is_section_filtered(filters, value, section="reject"):
            continue
        elif is_section_filtered(filters, value, section="accept"):
            yield idx, torrent
    yield None, None


def find_first_torrent(torrents: [TorrentFeedItem], filters={}):
    return next(find_torrent(torrents, filters))


async def updater(ctx: Context, feed: any, now: float):
    """Update Transmission torrents list from feeds"""
    feed_name = feed["name"]
    actives = [torrent for _, torrent in ctx.ls()]
    filters = prepare_filters(feed)
    added = 0
    removed = 0
    total = 0

    for feed_torrent in get_feed(ctx, feed):
        total += 1
        idx, _ = find_first_torrent(
            actives,
            filters={"accept": filters["accept"]},
        )
        if idx is not None:
            actives.pop(idx)
            continue
        if ctx.add(
            **feed_torrent.serialize(), paused=ctx.options.transmission["paused"]
        ):
            added += 1
    for idx, torrent in find_torrent(actives, filters=merge_filters_section(filters)):
        if idx is None:
            continue
        ctx.remove(
            torrent.id,
            delete_local_data=ctx.options.transmission["delete_local_data"],
        )
        removed += 1
    logger.debug(
        "[{}] added: {}, removed: {}, total: {}".format(
            feed_name, added, removed, total
        )
    )
