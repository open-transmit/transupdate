import re
from transupdate.logger import getLogger

logger = getLogger(__file__)

ALLOWED_SECTIONS = ["accept", "reject"]


def prepare_filters(feed):
    filters = {}
    for section in feed["filters"].keys():
        if section not in ALLOWED_SECTIONS:
            raise KeyError(section)
        filters[section] = [
            re.compile(".*{}.*".format(p), re.I) for p in feed["filters"][section]
        ]
    return filters


def merge_filters_section(filters, section="accept"):
    new_filters = {section: []}
    for _section in filters.keys():
        new_filters[section].extend(filters[_section])
    return new_filters


def is_section_filtered(filters, value, section="accept"):
    if section not in filters:
        return False
    for pattern in filters[section]:
        if pattern.match(value):
            return True
    return False


def filter_by_name(name: str, filters: any) -> bool:
    if not name.endswith(".torrent"):
        return False
    if "reject" in filters:
        for pattern in filters["reject"]:
            if pattern.match(name):
                return False
    if "accept" in filters:
        for pattern in filters["accept"]:
            if pattern.match(name):
                return True
    return False