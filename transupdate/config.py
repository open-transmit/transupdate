import os

import jsonschema

from transupdate.logger import getLogger
from transupdate.utils import read_json, resolve_path

logger = getLogger(__file__)

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
schemadir = os.path.join(basedir, "transupdate", "schemas")


class ConfigurationPathError(Exception):
    pass


configuration_paths = {
    "~/.config/transupdate.json",
    "/etc/transupdate.json",
    "./transupdate.json",
}


def make_conf_path(confpath=None):
    if confpath is not None:
        return confpath
    for confpath in configuration_paths:
        confpath = resolve_path(confpath)
        if os.path.exists(confpath):
            return confpath
    raise ConfigurationPathError("ConfigurationNotFound")


def read_config(confpath=None):
    confpath = make_conf_path(confpath=confpath)
    config = read_json(confpath)
    return schema_validate(instance=config)


read_schema = lambda: read_json(os.path.join(schemadir, "transupdate.schema.json"))

json_schema_resolver = lambda referrer=None: jsonschema.RefResolver(
    referrer=referrer, base_uri="file://{}/".format(schemadir)
)


def schema_validate(instance=None, schema=read_schema):
    if callable(schema):
        schema = read_schema()
    jsonschema.validate(
        instance, schema, resolver=json_schema_resolver(referrer=schema)
    )
    return instance
