# Transupdate

Update transmission torrent list from `RSS`, `HTML index` and `archives`.

The script can control remote `Transmission`, even for archives.

## install & run

## config

You should copy configuration from [transupdate.json](transupdate/templates/transupdate.json)

By default these folder are tested.

```python
    "~/.config/transupdate.json",
    "/etc/transupdate.json",
    "./transupdate.json",
```

```sh
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
./run
```

```sh
./scripts/run --debug  --confpath=~/.config/transupdate/transupdate.json
```

## cron

If `env`is a Python3 virtual environment with dependencies installed.

```sh
#!/bin/sh

TRANSUPDATE_PATH=/opt/usr/transupdate

cd $TRANSUPDATE_PATH
. env/bin/activate; ./scripts/run --debug --logfile=/var/log/transupdate.log --confpath=/etc/transupdate.json > /dev/null 2>&1
```

## daemon

The script can run as daemon and execute tasks at specified interval or be run from `cron`.

```sh
TRANSUPDATE_DAEMON=1 ... ./scripts/run
```

```sh
TRANSUPDATE_DATA=/PATH/TO/DATA ./scripts/run --daemon
```

## docker (local)

This will start transmission daemon and transupdate in the same container.

`DOCKERHUB_USER` environment variable must be set for the build.

Test image `showi/transupdate-torrent-daemon:0.0.1` (not reliable).

### build

```makefile
.PHONY: docker-build-local
docker-build-local: check-env
	docker build \
		--tag $(DOCKERHUB_USER)/$(TAG) \
		.
```

The command `build` call `build-march` than build image for different architecture.
See [common.mk](scripts/common.mk)

### run

Example from [Makefile](Makefile)

```makefile
.PHONY: docker-test-run
docker-test-run:
	mkdir -p $(TOPDIR)/env/transmission-etc
	cp -r $(TOPDIR)/etc/transmission $(TOPDIR)/env/transmission-etc/transmission
	cp -r $(TOPDIR)/etc/transupdate.json $(TOPDIR)/env/transmission-etc/transupdate.json
	TRANSUPDATE_CONF=etc/transupdate.json \
	TRANSUPDATE_DEBUG=1 \
	TRANSUPDATE_LOGFILE=transupdate.log \
	TRANSUPDATE_DRY=1 \
	DOCKER_RUN_OPTS="-v $(shell pwd)/transupdate/:/transupdate/transupdate -v $(shell pwd)/env/transmission-etc/transmission:/etc/transmission -v $(shell pwd)/env/transmission-etc/transupdate.json:/etc/transupdate.json" \
	$(MAKE) docker-run
```

### wrap it up wih `make`

```sh
make docker-build-local
TRANSUPDATE_DATA=/PATH/TO/TORRENTS/DL \
    TRANSUPDATE_CONF=/PATH/TO/CONF \
    make docker-run
```
