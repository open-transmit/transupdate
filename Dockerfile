FROM alpine:3.13.0 as image

ENV TRANSUPDATE_ROOT=/transupdate
ENV TRANSUPDATE_DATA=${TRANSUPDATE_ROOT}/data
ENV TRANSUPDATE_USER=transupdate

FROM image as pre_build
RUN apk add --no-cache --virtual .build-deps-1 \
    transmission-daemon \
    python3 \
    py3-pip
FROM pre_build
WORKDIR /transupdate
COPY transupdate/ transupdate
COPY requirements.txt ${TRANSUPDATE_ROOT}/
COPY scripts/ scripts/
COPY docker/etc/transupdate.json /etc/transupdate.json
RUN mkdir -p /etc/transmission
COPY docker/etc/transmission/settings.json /etc/transmission/settings.json
RUN pip3 install -r requirements.txt
RUN chmod u+x scripts/*
EXPOSE 51413
EXPOSE 9091

CMD [ "/transupdate/scripts/bootstrap" ]
